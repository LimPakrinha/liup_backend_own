<?php

namespace App;

class AppType extends MbsBaseModel
{
    protected $table = 'app_types';
    protected $fillable = [
        'id','app_name','status_id'
    ];

    public function __construct()
    {
        parent::__construct($this->table, $this->fillable);
    }
}
