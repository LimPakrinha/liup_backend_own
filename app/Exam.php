<?php

namespace App;

class Exam extends MbsBaseModel
{
    protected $table = 'exam';
    protected $fillable = [
        'status_seat_id','exam_type_id','name','exam_place','exam_date','exam_start_time','exam_end_time','register_open_date','register_close_date','announce_score_date','cost','seat','is_active'
    ];
    protected $hidden = [
        'created_at', 'updated_at', 'status_id'
    ];

    public function __construct()
    {
        parent::__construct($this->table, $this->fillable);
    }
}
