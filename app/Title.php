<?php

namespace App;

class Title extends MbsBaseModel
{
    protected $table = 'title';
    protected $fillable = [
        'name'
    ];
    protected $hidden = [
        'created_at', 'updated_at', 'status_id'
    ];

    public function __construct()
    {
        parent::__construct($this->table, $this->fillable);
    }
}
