<?php

namespace App;

class Province extends MbsBaseModel
{
    protected $table = 'province';
    protected $fillable = [
        'id','prov_name','region_id','status_id'
    ];

    public function __construct()
    {
        parent::__construct($this->table, $this->fillable);
    }
}
