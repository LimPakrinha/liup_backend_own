<?php

namespace App;

class Level extends MbsBaseModel
{
    protected $table = 'level';
    protected $fillable = [
        'id','level_name'
    ];
    protected $hidden = [
        'created_at', 'updated_at','status_id'
    ];

    public function __construct()
    {
        parent::__construct($this->table, $this->fillable);
    }
}
