<?php

namespace App;

class Subdistrict extends MbsBaseModel
{
    protected $table = 'sub_district';
    protected $fillable = [
        'id','sub_district_name','district_id','status_id'
    ];

    public function __construct()
    {
        parent::__construct($this->table, $this->fillable);
    }
}
