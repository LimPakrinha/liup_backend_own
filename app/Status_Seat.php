<?php

namespace App;

class Status_Seat extends MbsBaseModel
{
    protected $table = 'status_seat';
    protected $fillable = [
        'id','name','status_id'
    ];

    public function __construct()
    {
        parent::__construct($this->table, $this->fillable);
    }
}
