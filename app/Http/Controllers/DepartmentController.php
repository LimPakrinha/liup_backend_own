<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use App\Department;

class DepartmentController extends MbsBaseController
{
    public function __construct()
    {
        parent::__construct(new department());
    }

    public function getDepartmentData($fid)
    {
        $data = $this->model::where('faculty_id', $fid)
        ->orderBy('depart_name', 'asc')
        ->get();

    return $this->responseRequestSuccess($data);
    }

    public function getDataPaginate(Request $request, $limit)
    {
        $request->request->add([
            'status_id' => Status::$ACTIVE
        ]);

        return parent::getDataPaginate($request, $limit);
    }
}
