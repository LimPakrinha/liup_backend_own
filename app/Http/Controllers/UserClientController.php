<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Status;
use App\User;
use App\Subdistrict;
use App\Department;
use App\Level;

class UserClientController extends MbsBaseController
{
    public function __construct()
    {
        parent::__construct(new User());
    }
    public function getAllUserClient(Request $request)
    {
        $current_page = $request->pageNumber + 1;
        Paginator::currentPageResolver(function () use ($current_page) {
            return $current_page;
        });
        $model_query = $this->model::join('card_types', 'card_types.id', 'user.card_type_id')
            ->select(
                'user.*',
                'card_types.card_name As card_type_name'
            )
            ->where('user.user_type_id', 2)
            ->where('user.status_id', Status::$ACTIVE);

        if (!empty($request->sortField)) {
            $model_query->orderBy($request->sortField, $request->sortOrder);
        }

        if (!empty($request->filter)) {
            $model_query->where(function ($query) use ($request) {
                foreach ($request->filter as $key => $value) {
                    $query->orWhere($key, 'Like', '%' . $value . '%');
                }
            });
        }

        $user_client = $model_query->paginate($request->pageSize);


        return $this->responseRequestSuccess($user_client);
    }

    public function getClientDetail($id)
    {
        $data = $this->model::join('card_types', 'card_types.id', 'user.card_type_id')
            ->join('app_types', 'app_types.id', 'user.app_type_id')
            ->join('title', 'title.id', 'user.title_id')
            ->select(
                'user.*',
                'card_types.card_name As card_type_name',
                'app_types.app_name As app_type_name',
                'title.name As title_name')
            ->where('user.status_id', Status::$ACTIVE)
            ->where('user.user_type_id', 2)
            ->find($id);

        $address_data = Subdistrict::join('district', 'district.id', 'sub_district.district_id')
            ->join('province', 'province.id', 'district.province_id')
            ->join('zipcode','zipcode.sub_district_id','sub_district.id')
            ->where('sub_district.id',$data->sub_district_id)
            ->select(
                'sub_district.sub_district_name As sub_district',
                'zipcode.zipcode As zipcode',
                'district.dist_name As district',
                'district.id As dis_id',
                'province.prov_name As province',
                'province.id As prov_id')
            ->first();
        $department_data = Department::join('faculty','faculty.id','department.faculty_id')
                ->select(
                    'faculty.id AS faculty_id',
                    'faculty.faculty_name',
                    'department.id AS department_id',
                    'department.depart_name')
                ->where('department.id',$data->department_id)
                ->where('department.status_id', Status::$ACTIVE)
                ->where('faculty.status_id', Status::$ACTIVE)
                ->first();
        $level_data = Level::where('id',$data->level_id)
                ->where('status_id', Status::$ACTIVE)
                ->select(
                    'id AS level_id',
                    'level_name')
                ->first();
        if($address_data)
            $data = array_merge($data->toArray(), $address_data->toArray());
        if($department_data)
            $data = array_merge($data->toArray(), $department_data->toArray(), $level_data->toArray());
        return $this->responseRequestSuccess($data);
    }
}
