<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use App\Title;

class titleController extends MbsBaseController
{
    public function __construct()
    {
        parent::__construct(new title());
    }

    public function getAll(Request $request)
    {
        $request->request->add([
            'status_id' => Status::$ACTIVE
        ]);

        return parent::getAll($request);
    }

    public function getDataPaginate(Request $request, $limit)
    {
        $request->request->add([
            'status_id' => Status::$ACTIVE
        ]);

        return parent::getDataPaginate($request, $limit);
    }
}
