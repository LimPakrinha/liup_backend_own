<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use App\Status_Seat;

class Status_SeatController extends MbsBaseController
{
    public function __construct()
    {
        parent::__construct(new Status_Seat());
    }

}
