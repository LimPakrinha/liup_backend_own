<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use App\ExamType;

class ExamTypeController extends MbsBaseController
{
    public function __construct()
    {
        parent::__construct(new ExamType());
    }
}
