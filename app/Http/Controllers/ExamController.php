<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Status;
use App\Exam;
use App\UserExam;

class ExamController extends MbsBaseController
{
    public function __construct()
    {
        parent::__construct(new Exam());
    }

    public function getDataPaginate(Request $request, $limit)
    {
        $request->request->add([
            'status_id' => Status::$ACTIVE
        ]);

        return parent::getDataPaginate($request, $limit);
    }

    public function getData($id)
    {
        $data = $this->model::join('status_seat', 'status_seat.id', 'exam.status_seat_id')
            ->select('exam.*', 'status_seat.name as status_seat_name')
            ->where('exam.status_id', Status::$ACTIVE)
            ->find($id);

        return $this->responseRequestSuccess($data);
    }
    public function getAll(Request $request){
        $condition = $this->setFilterCondition($request);
            $data = $this->model::join('status_seat', 'status_seat.id', 'exam.status_seat_id')
            ->join('exam_type','exam_type.id','exam.exam_type_id')
            ->select('exam.*', 'status_seat.name as status_seat_name','exam_type.name as exam_type_name')
            ->orderBy('exam.created_at', 'asc')
            ->where('exam.status_id', Status::$ACTIVE)
            ->get();

        return $this->responseRequestSuccess($data);
    }
// for Admin side
    public function getAllExam(Request $request)
    {
        $current_page = $request->pageNumber + 1;
        Paginator::currentPageResolver(function () use ($current_page) {
            return $current_page;
        });
        $model_query = $this->model::join('status_seat', 'status_seat.id', 'exam.status_seat_id')
            ->join('exam_type','exam_type.id','exam.exam_type_id')
            ->select('exam.*', 'status_seat.name as status_seat_name','exam_type.name as exam_type_name')
            ->orderBy('exam.created_at', 'asc')
            ->where('exam.status_id', Status::$ACTIVE);

        if (!empty($request->sortField)) {
            $model_query->orderBy($request->sortField, $request->sortOrder);
        }

        if (!empty($request->filter)) {
            $model_query->where(function ($query) use ($request) {
                foreach ($request->filter as $key => $value) {
                    $query->orWhere($key, 'Like', '%' . $value . '%');
                }
            });
        }

        $exam = $model_query->paginate($request->pageSize);


        return $this->responseRequestSuccess($exam);
    }
//--------------

// for user side
    public function getAllUserRegister(Request $request)
    {
        $current_page = $request->pageNumber + 1;
        Paginator::currentPageResolver(function () use ($current_page) {
            return $current_page;
        });
        $model_query = $this->model::join('status_seat', 'status_seat.id', 'exam.status_seat_id')
            ->select(
                'exam.*',
                'status_seat.name AS status_seat_name'
            )
            ->where('exam.is_active', 1)
            ->where('exam.status_id', Status::$ACTIVE);

        if (!empty($request->sortField)) {
            $model_query->orderBy($request->sortField, $request->sortOrder);
        }

        if (!empty($request->filter)) {
            $model_query->where(function ($query) use ($request) {
                foreach ($request->filter as $key => $value) {
                    $query->orWhere($key, 'Like', '%' . $value . '%');
                }
            });
        }

        $exam_user_register = $model_query->paginate($request->pageSize);

        foreach ($exam_user_register as $exam) {
            $user_exam = UserExam::where('status_id', Status::$ACTIVE)
                ->where('user_id', \Auth::user()->id)
                ->where('exam_id', $exam->id)
                ->first();
            
            $exam->receipt_file = "";
            $exam->description = "";
            if (!empty($user_exam)) {
                $exam->user_exam_id = $user_exam->id;
                if ($user_exam->is_reg_confirmed == 1) {
                    // pending for payment
                    $exam->register_process = 1;
                } else if($user_exam->is_reg_confirmed == 2) {
                    // payment verification pending
                    $exam->register_process = 2;
                    $exam->receipt_file = $user_exam->receipt_file;
                } else if($user_exam->is_reg_confirmed == 3){
                    // payment denied
                    $exam->description = $user_exam->description;
                    $exam->register_process = 3;
                } else {
                    // payment confirmed
                    $exam->register_process = 4;
                }
            } else {
                // not register
                $exam->register_process = 0;
            }

            $total_exam_registered = UserExam::where('status_id', Status::$ACTIVE)
                ->where('exam_id', $exam->id)
                ->where('is_reg_confirmed', 4)
                ->count();


            $exam->remain_seat = $exam->seat - $total_exam_registered;

            if ($exam->remain_seat < 0) {
                $exam->remain_seat = 0;
            }
        }
        return $this->responseRequestSuccess($exam_user_register);
    }
   
}
