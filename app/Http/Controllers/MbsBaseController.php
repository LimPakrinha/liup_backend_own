<?php

namespace App\Http\Controllers;

use App\Status;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class MbsBaseController extends Controller
{
    protected $model;

    public function __construct($model = null)
    {
        $this->model = $model;
    }

    public function index()
    {
        $data = $this->model::where('status_id', Status::$ACTIVE)->get();

        return $this->responseRequestSuccess($data);
    }

    public function getAll(Request $request)
    {
        $condition = $this->setFilterCondition($request);
        $data = $this->model::where($condition)
            ->orderBy('created_at', 'asc')
            ->get();
            
        return $this->responseRequestSuccess($data);
    }

    public function getDataPaginate(Request $request, $limit)
    {
        $condition = $this->setFilterCondition($request);
        $data = $this->model::where($condition)
            ->orderBy('created_at', 'asc')
            ->paginate($limit);

        return $this->responseRequestSuccess($data);
    }

    public function getDataPaginateModelQuery(Request $request, $model_query = null)
    {
        $current_page = $request->pageNumber + 1;
        Paginator::currentPageResolver(function () use ($current_page) {
            return $current_page;
        });

        if (empty($model_query)) {
            $model_query = $this->model::where('status_id', Status::$ACTIVE)
                ->select('*');
        }

        if (!empty($request->sortField)) {
            $model_query->orderBy($request->sortField, $request->sortOrder);
        }

        if (!empty($request->filter)) {
            $model_query->where(function ($query) use ($request) {
                foreach ($request->filter as $key => $value) {
                    $query->orWhere($key, 'Like', '%' . $value . '%');
                }
            });
        }

        return $this->responseRequestSuccess($model_query->paginate($request->pageSize));
    }

    public function getData($id)
    {
        $data = $this->model::where('status_id', Status::$ACTIVE)
            ->find($id);

        return $this->responseRequestSuccess($data);
    }

    public function addData(Request $request)
    {
        $setArray = $request->request;
        $this->model->setData($setArray);

        if ($this->model->save()) {
            return $this->responseRequestSuccess($this->model);
        } else {
            return $this->responseRequestError("Cannot create " . $this->model->getTable());
        }
    }

    public function updateData(Request $request, $id)
    {
        $getData = $this->model::where('id', $id)
            ->where('status_id', Status::$ACTIVE)
            ->first();

        if (!empty($getData)) {
            $setArray = $request->request;
            $getData->setData($setArray);

            if ($getData->save()) {
                return $this->responseRequestSuccess($getData);
            } else {
                return $this->responseRequestError("Cannot update " . $this->model->getTable());
            }
        } else {
            return $this->responseRequestError("Cannot find " . $this->model->getTable());
        }
    }

    public function deleteData($id)
    {
        $data = $this->model::findOrFail($id);
        $data->status_id = Status::$DELETED;
        $data->save();

        return $this->responseRequestSuccess($data);
    }

    public function deleteMutipleData(Request $request)
    {
        foreach ($request->id as $id) {
            $this->model::where('status_id', Status::$ACTIVE)
                ->where('id', $id)
                ->update(['status_id' => Status::$DELETED]);
        }

        return $this->responseRequestSuccess("Delete multiple data success");
    }

    public function setDataBulkByUserId($dataList, $user_id)
    {
        $this->model::where('user_id', $user_id)
            ->delete();
        $ret = $this->model::insert($dataList);

        return $ret;
    }

    protected function responseRequestSuccess($ret)
    {
        return response()->json(['status' => 'success', 'data' => $ret], 200)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }

    protected function responseRequestError($message = 'Bad request', $statusCode = 200)
    {
        return response()->json(['status' => 'error', 'error' => $message], $statusCode)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }

    protected function strRandom($length = 6)
    {
        return substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'), 0, $length);
    }

    protected function setFilterCondition(Request $request)
    {
        $condition = [];
        $request = $request->all();

        foreach ($request as $field => $value) {
            array_push($condition, [$field, $value]);
        }

        return $condition;
    }

    protected function removeOldUploadImage($destination_path, $file_path)
    {
        $old_image_arr = explode('/', $file_path);
        $old_image = end($old_image_arr);

        if ($old_image && file_exists($destination_path . $old_image)) {
            unlink($destination_path . $old_image);
        }
    }

    protected function saveImage($base64_image, $subPath)
    {
        if (!empty($base64_image)) {
            $img_uniqid = uniqid('', true);
            $image = $base64_image;
            $destinationPath = "../public/upload/" . $subPath;

            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0777, true);
            }

            $imgData = substr($image, 1 + strrpos($image, ","));

            if (file_put_contents("./upload/" . $subPath . "/" . $subPath . "-" . $img_uniqid . ".png", base64_decode($imgData)) != null) {
                return "/upload/" . $subPath . "/" . $subPath . "-" . $img_uniqid . ".png";
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function removeFile($image_path)
    {
        if (!empty($image_path) && file_exists('.' . $image_path)) {
            if (unlink('.' . $image_path)) {
                return true;
            } else {
                return false;
            }
        }
    }
}
