<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use App\CardType;

class cardtypeController extends MbsBaseController
{
    public function __construct()
    {
        parent::__construct(new CardType());
    }

}
