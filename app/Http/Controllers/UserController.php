<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Firebase\JWT\JWT;
use App\Status;
use App\User;
use App\UserType;
use App\UserUserType;

class UserController extends MbsBaseController
{
    public function __construct()
    {
        parent::__construct(new User());
       // parent::__construct(new UserUserType());
    }
    private function jwt(User $user)
    {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + env('JWT_EXPIRE_HOUR') * 60 * 60 // Expiration time
        ];
        return JWT::encode($payload, "JhbGciOiJIUzI1N0eXAiOiJKV1QiLC");
    }
    //return JWT::encode($payload, env('JWT_SECRET'));
    //return JWT::encode($payload, "JhbGciOiJIUzI1N0eXAiOiJKV1QiLC");
    public function login(Request $request)
    {
        $user = User::
        // where('card_no', $request->card_no)
            where(function ($query) use ($request) {
                $query->where('card_no', $request->card_no)
                    ->orWhere('stu_id', $request->card_no);
                })
            ->where('status_id', Status::$ACTIVE)
            ->first();
//----Original code
// if (!empty($user) && Hash::check($request->password, $user->password)) {
//     $user_user_type = UserType::join('user_user_type', 'user_type.id', 'user_user_type.user_type_id')
//         ->where('user_user_type.user_id', $user->id)
//         ->where('user_user_type.status_id', Status::$ACTIVE)
//         ->get();

//     $user['api_token'] = $this->jwt($user);
//     $user['user_user_type'] = $user_user_type;

//     return $this->responseRequestSuccess($user);
// } 
//-------------------
        if (!empty($user) && Hash::check($request->password, $user->password)) {
            $user_user_type = User::where('user_type_id', $user->user_type_id)
                ->where('status_id', Status::$ACTIVE)
                ->get();

            $user['api_token'] = $this->jwt($user);
            $user['user_user_type'] = $user_user_type;

            return $this->responseRequestSuccess($user);
        } 
        else {
            return $this->responseRequestError('ชื่อบัญชีผู้ใช้หรือรหัสผ่านของคุณไม่ถูกต้อง');
        }
    }

    public function forgotPassword(Request $request)
    {
        $user = User::where('email', $request->email)
            ->where('status_id', Status::$ACTIVE)
            ->first();

        if (!empty($user)) {
            $raw_rpk = 'reset_password|' . $user->email . '|' . $user->id;
            $rpk = Crypt::encryptString($raw_rpk);
            $template_html = 'mail.forgot_password';
            $template_data = [
                'url_reset_password' => url('/user/reset_password/' . $rpk),
            ];

            Mail::send($template_html, $template_data, function ($msg) use ($user) {
                $msg->subject('ลืมรหัสผ่าน');
                $msg->to([$user->email]);
                $msg->from('agriconnect.mulberrysoft@gmail.com', 'Mulberrysoft');
            });

            return $this->responseRequestSuccess('ระบบได้ส่งขั้นตอนการแก้ไขรหัสผ่านไปยังอีเมล์ของคุณแล้ว');
        } else {
            return $this->responseRequestError('ไม่พบอีเมล์ของคุณในระบบ');
        }
    }

    public function resetPassword($rpk)
    {
        try {
            $rpk_raw = Crypt::decryptString($rpk);
            $rpk_arr = explode('|', $rpk_raw);

            if (count($rpk_arr) == 3) {
                $email = $rpk_arr[1];
                $user_id = $rpk_arr[2];
                $user = User::where('email', $email)
                    ->where('status_id', Status::$ACTIVE)
                    ->find($user_id);

                if (!empty($user)) {
                    $temp_password = $this->strRandom();
                    $user->password = Hash::make($temp_password);

                    if ($user->save()) {
                        $template_html = 'mail.reset_password';
                        $template_data = [
                            'temp_password' => $temp_password,
                        ];

                        Mail::send($template_html, $template_data, function ($msg) use ($user) {
                            $msg->subject('รหัสผ่านชั่วคราวระบบสมัครสอบและอบรมออนไลน์');
                            $msg->to([$user->email]);
                            $msg->from('agriconnect.mulberrysoft@gmail.com', 'Mulberrysoft');
                        });

                        return 'ระบบได้ทำการเปลี่ยนรหัสผ่านชั่วคราวให้คุณแล้ว กรุณาตรวจสอบอีเมล์เพื่อนำรหัสผ่านชั่วคราวมาใช้ในการเข้าสู่ระบบบัญชีของคุณ';
                    } else {
                        return 'เกิดข้อผิดพลาด ไม่สามารถอัพเดทข้อมูลได้';
                    }
                } else {
                    return 'ไม่พบข้อมูล';
                }
            } else {
                return 'ไม่พบข้อมูล';
            }
        } catch (DecryptException $e) {
            return 'ไม่พบข้อมูล';
        }
    }

    public function addData(Request $request)
    {
        //check to make it unique
        $validator = \Validator::make($request->all(), [
            'email' => 'required|email|unique:user',
            'card_no' => 'required|unique:user',
            'stu_id' => 'unique:user'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            return $this->responseRequestError($errors);

        } 
        
        else {
            $user = $this->model;
            $user->setData($request->request);
            $user->password = Hash::make($request->password);

            // if (!empty($request->profile_image)) {
            //     $user->profile_image = $this->saveImage($request->profile_image, 'user');
            // }
            if ($user->save()) {
                if (!empty($request->user_type_ids)) {

                    foreach ($request->user_type_ids as $user_type_id) { 
                        $user_user_type = new UserUserType();
                        $user_user_type->user_id = $user->id;
                        $user_user_type->user_type_id = $user_type_id;
                        $user_user_type->save();
                    }
                }
                return $this->responseRequestSuccess($user);

            } else {
                //$this->removeFile($user->profile_image);
                return $this->responseRequestError('ไม่สามารถบันทึกข้อมูล ' . $user->getTable());
            }
        }
    }

    public function updateData(Request $request, $id)
    {
        $validate_rule = [];
        $user = $this->model::where('status_id', Status::$ACTIVE)
            ->find($id);

        if (!empty($user)) {
            if ($user->email !== $request->email) {
                $validate_rule['email'] = 'required|email|unique:user';
            }

            // if ($user->card_no !== $request->card_no) {
            //     $validate_rule['card_no'] = 'required|unique:user';
            // }

            $validator = \Validator::make($request->all(), $validate_rule);

            if ($validator->fails()) {
                $errors = $validator->errors();
                return $this->responseRequestError($errors);
            } else {
                $old_image = $user->profile_image;
                $user->setData($request->request);

                if (!empty($request->password)) {
                    $user->password = Hash::make($request->password);
                }

                if (!empty($request->profile_image)) {
                    $user->profile_image = $this->saveImage($request->profile_image, 'user');
                    $this->removeFile($old_image);
                }

                if ($user->save()) {
                    $user_user_type = UserUserType::where('status_id', Status::$ACTIVE)
                        ->where('user_id', $id)
                        ->update([
                            'status_id' => Status::$DELETED
                        ]);

                    if (!empty($request->user_type_ids)) {
                        foreach ($request->user_type_ids as $user_type_id) {
                            $user_user_type = new UserUserType();
                            $user_user_type->user_id = $user->id;
                            $user_user_type->user_type_id = $user_type_id;
                            $user_user_type->save();
                        }
                    }

                    return $this->responseRequestSuccess($user);
                } else {
                    $this->removeFile($user->profile_image);

                    return $this->responseRequestError('ไม่สามารถบันทึกข้อมูล ' . $user->getTable());
                }
            }
        } else {
            return $this->responseRequestError('ไม่พบข้อมูลผู้ใช้งาน');
        }
    }

    public function updateImageProfile(Request $request, $id)
    {
        $user = User::where('status_id', Status::$ACTIVE)->find($id);

        if (!empty($user)) {
            if ($request->hasFile('profile_image')) {
                $original_filename = $request->file('profile_image')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
                $destination_path = './upload/user/';
                $profile_image = 'U-' . time() . '-' . $id . '.' . $file_ext;
                $this->removeOldUploadImage($destination_path, $user->profile_image);

                if ($request->file('profile_image')->move($destination_path, $profile_image)) {
                    $user->profile_image = '/upload/user/' . $profile_image;

                    if ($user->save()) {
                        return $this->responseRequestSuccess($user);
                    } else {
                        return $this->responseRequestError('Cannot update user');
                    }
                } else {
                    return $this->responseRequestError('Cannot upload file');
                }
            } else {
                return $this->responseRequestError('File not found');
            }
        } else {
            return $this->responseRequestError('User not found');
        }
    }

    public function updatePwd(Request $request, $id)
    {
        $validate_rule = [];
        $user = $this->model::where('status_id', Status::$ACTIVE)
            ->find($id);

        if (!empty($user)) {
            $validator = \Validator::make($request->all(), $validate_rule);

            if ($validator->fails()) {
                $errors = $validator->errors();
                return $this->responseRequestError($errors);
            } else {
                if (!empty($request->password)) {
                    $user->password = Hash::make($request->password);
                }         
                $user->setData($request->request);


                if ($user->save()) {
                    return $this->responseRequestSuccess($user);
                } else {
                    return $this->responseRequestError('ไม่สามารถบันทึกข้อมูล ' . $user->getTable());
                }
            }
        } 
        else {
            return $this->responseRequestError('ไม่พบข้อมูลผู้ใช้งาน');
        }
    }
}
