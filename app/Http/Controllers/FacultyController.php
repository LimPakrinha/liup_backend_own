<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use App\Faculty;

class facultyController extends MbsBaseController
{
    public function __construct()
    {
        parent::__construct(new faculty());
    }

    public function getFaculty(Request $request)
    {
        $condition = $this->setFilterCondition($request);
        $data = $this->model::where($condition)
            ->orderBy('faculty_name', 'asc')
            ->where('status_id', Status::$ACTIVE)
            ->get();
            
        return $this->responseRequestSuccess($data);
    }
}
