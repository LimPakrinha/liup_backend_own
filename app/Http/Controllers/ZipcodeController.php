<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use App\Zipcode;

class ZipcodeController extends MbsBaseController
{
    public function __construct()
    {
        parent::__construct(new Zipcode());
    }

    public function getZipData($sid)
    {
        $data = $this->model::where('sub_district_id', $sid)
        ->select("id","zipcode")->get();

       //$data = $this->model::select("SELECT * FROM zipcode");

    return $this->responseRequestSuccess($data);
    //return $data;
            //->where('province_id', $id);
    }
}
