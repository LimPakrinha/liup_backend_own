<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Status;
use App\StatusSeat;
use App\Exam;
use App\UserExam;

class UserExamController extends MbsBaseController
{
    public function __construct()
    {
        parent::__construct(new UserExam());
    }

    public function getAll(Request $request)
    {
        $user_exam = $this->model::join('exam', 'exam.id', 'user_exam.exam_id')
            ->join('user', 'user.id', 'user_exam.user_id')
            ->join('gender', 'gender.id', 'user.gender_id')
            ->select(
                'user_exam.*',
                'user.gender_id AS user_gender_id',
                'gender.name AS gender_name',
                'user.first_name AS user_first_name',
                'user.last_name AS user_last_name',
                'user.email AS user_email',
                'user.phone_number AS user_phone_number'
            )
            ->where('user_exam.status_id', Status::$ACTIVE)
            ->get();

        return $this->responseRequestSuccess($user_exam);
    }

    public function getDataPaginate(Request $request, $limit)
    {
        $request->request->add([
            'status_id' => Status::$ACTIVE
        ]);

        return parent::getDataPaginate($request, $limit);
    }

    public function getExamUser($id)
    {
        $data = $this->model::join('user', 'user.id', 'user_exam.user_id')
        
            ->select('user_exam.*', 'user.first_name as first_name', 'user.last_name as last_name')
            ->where('user_exam.status_id', Status::$ACTIVE)
            ->where('user_exam.exam_id', $id)->get();
        $exam_user_register = $data;
        foreach ($exam_user_register as $data) {
            $total_exam_registered = UserExam::where('status_id', Status::$ACTIVE)
                ->where('exam_id', $id)
                ->where('is_reg_confirmed', 1)
                ->count();
            $data['registered_seat']= $total_exam_registered;
        }
        return $this->responseRequestSuccess($exam_user_register);
    }

    public function getExamRegConfirmed($id)
    {
        $data = $this->model::where('status_id', Status::$ACTIVE)
            ->select('is_reg_confirmed')->find($id);

        return $this->responseRequestSuccess($data);
    }

    public function getDataPaginateModelQuery(Request $request, $model_query = null)
    {
        $model_query = $this->model::join('exam', 'exam.id', 'user_exam.exam_id')
            ->join('user', 'user.id', 'user_exam.user_id')
            ->join('gender', 'gender.id', 'user.gender_id')
            ->select(
                'user_exam.*',
                'user.gender_id AS user_gender_id',
                'gender.name AS gender_name',
                'user.first_name AS user_first_name',
                'user.last_name AS user_last_name',
                'user.email AS user_email',
                'user.phone_number AS user_phone_number'
            );

        return parent::getDataPaginateModelQuery($request, $model_query);
    }

    public function getData($id)
    {
        $user_exam = $this->model::join('exam', 'exam.id', 'user_exam.exam_id')
            ->join('user', 'user.id', 'user_exam.user_id')
            ->join('gender', 'gender.id', 'user.gender_id')
            ->select(
                'user_exam.*',
                'user.gender_id AS user_gender_id',
                'gender.name AS gender_name',
                'user.first_name AS user_first_name',
                'user.last_name AS user_last_name',
                'user.email AS user_email',
                'user.phone_number AS user_phone_number'
            )
            ->where('user_exam.status_id', Status::$ACTIVE)
            ->find($id);

        return $this->responseRequestSuccess($user_exam);
    }

    public function addData(Request $request)
    {
        $exam = Exam::where('status_id', Status::$ACTIVE)
            ->find($request->exam_id);

        if ($exam->cost == 0) {
            $request->merge(['is_reg_confirmed' => 4]);
        }

        $request->request->add([
            'user_id' => \Auth::user()->id
        ]);

        return parent::addData($request);
    }

    public function getUserExam(Request $request){
        $current_page = $request->pageNumber + 1;
        Paginator::currentPageResolver(function () use ($current_page) {
            return $current_page;
        });
        $model_query = $this->model::join('exam', 'user_exam.exam_id', 'exam.id')
            ->select(
                'user_exam.id',
                'user_exam.seat_no',
                'user_exam.is_pass',
                'exam.name',
                'exam.exam_date',
                'user_exam.score',
                'exam.register_open_date',
                'exam.register_close_date',
                'exam.announce_score_date',
                'exam.cost',
                'exam.exam_place',
                'exam.exam_start_time',
                'exam.exam_end_time'
            )
            ->where('user_exam.user_id', \Auth::user()->id)
            ->where('user_exam.is_reg_confirmed', 4);

        if (!empty($request->sortField)) {
            $model_query->orderBy($request->sortField, $request->sortOrder);
        }

        if (!empty($request->filter)) {
            $model_query->where(function ($query) use ($request) {
                foreach ($request->filter as $key => $value) {
                    $query->orWhere($key, 'Like', '%' . $value . '%');
                }
            });
        }

        $user_exam = $model_query->paginate($request->pageSize);


        return $this->responseRequestSuccess($user_exam);
    }

    public function getUserExamScore($id, Request $request)
    {
        // $data = $this->model::join('user', 'user.id', 'user_exam.user_id')
        //     ->join('exam', 'exam.id', 'user_exam.exam_id')
        //     ->select('user_exam.*', 
        //             'user.first_name as user_exam_first_name',
        //             'user.last_name as user_exam_last_name',
        //             'user.card_no as user_exam_card_no',
        //             'exam_start_time',
        //             'exam_end_time'
        //             )
        //     ->where([
        //         ['user_exam.status_id', Status::$ACTIVE],
        //         ['user_exam.exam_id', $id]
        //     ])
        //     ->get();

        // return $this->responseRequestSuccess($data);
        $current_page = $request->pageNumber + 1;
        Paginator::currentPageResolver(function () use ($current_page) {
            return $current_page;
        });
        $model_query = $this->model::join('user', 'user.id', 'user_exam.user_id')
            ->join('exam', 'exam.id', 'user_exam.exam_id')
            ->select('user_exam.*', 
                    'user.first_name as user_exam_first_name',
                    'user.last_name as user_exam_last_name',
                    'user.card_no as user_exam_card_no',
                    'exam_start_time',
                    'exam_end_time'
                    )
            ->where([
                ['user_exam.status_id', Status::$ACTIVE],
                ['user_exam.exam_id', $id]
            ]);

        if (!empty($request->sortField)) {
            $model_query->orderBy($request->sortField, $request->sortOrder);
        }

        if (!empty($request->filter)) {
            $model_query->where(function ($query) use ($request) {
                foreach ($request->filter as $key => $value) {
                    $query->orWhere($key, 'Like', '%' . $value . '%');
                }
            });
        }

        $user_exam_score = $model_query->paginate($request->pageSize);


        return $this->responseRequestSuccess($user_exam_score);
    }
    public function getUserExamScoreDetail($id)
    {
        $data = $this->model::join('user', 'user.id', 'user_exam.user_id')
            ->join('exam', 'exam.id', 'user_exam.exam_id')
            ->select('user_exam.*',
                    'exam.name as exam_name',
                    'exam.exam_date',
                    'user.first_name as user_exam_first_name',
                    'user.last_name as user_exam_last_name',
                    'user.card_no as user_exam_card_no',
                    'user.card_type_id'
                    )
            ->where([
                ['user_exam.status_id', Status::$ACTIVE],
                // ['user_exam.user_id', $id]
            ])
            ->find($id);
            // ->get();

        return $this->responseRequestSuccess($data);
    }
    // protected function responseRequestSuccessOfExamUser($ret,$regS)
    // {
    //     return response()->json(['status' => 'success', 'data' => $ret,'registered_seat'=> $regS], 200)
    //         ->header('Access-Control-Allow-Origin', '*')
    //         ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    // }
    public function updateData(Request $request, $id){
        if (!empty($request->receipt_file)) {
            $request->request->add([
                'receipt_file' => $this->saveImage($request->receipt_file, 'upload_slip')
            ]);
        }
        return parent::updateData($request, $id);
    }
}
