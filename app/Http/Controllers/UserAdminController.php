<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Hash;
use App\Status;
use App\User;
use App\UserUserType;


class UserAdminController extends MbsBaseController
{
    public function __construct()
    {
        parent::__construct(new User());
    }

    public function getAll(Request $request)
    {
        $request->request->add([
            'status_id' => Status::$ACTIVE
        ]);

        return parent::getAll($request);
    }

    public function getUserAdminData($id)
    {
        $data = $this->model::where('status_id', Status::$ACTIVE)
            ->find($id);

        return $this->responseRequestSuccess($data);
        
    }

    public function getUser($id,Request $request)
    {
        // $id=1;
        // $data = $this->model::where('id', '!=' , $id)
        // ->where('user_type_id',1)
        // ->where('status_id', Status::$ACTIVE)
        // ->get();
        // return $this->responseRequestSuccess($data);
        $current_page = $request->pageNumber + 1;
        Paginator::currentPageResolver(function () use ($current_page) {
            return $current_page;
        });
        $model_query = $this->model::where('id', '!=' , $id)
        ->where('user_type_id',1)
        ->where('status_id', Status::$ACTIVE);

        if (!empty($request->sortField)) {
            $model_query->orderBy($request->sortField, $request->sortOrder);
        }

        if (!empty($request->filter)) {
            $model_query->where(function ($query) use ($request) {
                foreach ($request->filter as $key => $value) {
                    $query->orWhere($key, 'Like', '%' . $value . '%');
                }
            });
        }

        $user_admin = $model_query->paginate($request->pageSize);


        return $this->responseRequestSuccess($user_admin);
    }

    public function updateData(Request $request, $id)
    {
        $validate_rule = [];
        $user = $this->model::where('status_id', Status::$ACTIVE)
            ->find($id);

        if (!empty($user)) {
            if ($user->email !== $request->email) {
                $validate_rule['email'] = 'email|unique:user';
            }


            $validator = \Validator::make($request->all(), $validate_rule);

            if ($validator->fails()) {
                $errors = $validator->errors();
                return $this->responseRequestError($errors);
            } else {
                

                if (!empty($request->password)) {
                    $user->password = Hash::make($request->password);
                    
                }
                $user->setData($request->request);
                

                if ($user->save()) {
                    $user_user_type = UserUserType::where('status_id', Status::$ACTIVE)
                        ->where('user_id', $id)
                        ->update([
                            'status_id' => Status::$DELETED
                        ]);

                    if (!empty($request->user_type_ids)) {
                        foreach ($request->user_type_ids as $user_type_id) {
                            $user_user_type = new UserUserType();
                            $user_user_type->user_id = $user->id;
                            $user_user_type->user_type_id = $user_type_id;
                            $user_user_type->save();
                        }
                    }

                    return $this->responseRequestSuccess($user);
                } else {
                    $this->removeFile($user->profile_image);

                    return $this->responseRequestError('ไม่สามารถบันทึกข้อมูล ' . $user->getTable());
                }
            }
        } else {
            return $this->responseRequestError('ไม่พบข้อมูลผู้ใช้งาน');
        }
    }

    // public updatepassword(Request $request, $id){
    //     $validate_rule = [];
    //     $user = $this->model::where('status_id', Status::$ACTIVE)
    //         ->find($id);

    //     if (!empty($user)) {
    //         if ($user->email !== $request->email) {
    //             $validate_rule['email'] = 'required|email|unique:user';
    //         }


    //         $validator = \Validator::make($request->all(), $validate_rule);

    //         if ($validator->fails()) {
    //             $errors = $validator->errors();
    //             return $this->responseRequestError($errors);
    //         } else {
                

    //             if (!empty($request->password)) {
    //                 $user->password = Hash::make($request->password);
    //                 $user->setData($request->request);
    //             }
                
    //             else {
    //                 return $this->responseRequestError('ไม่สามารถบันทึกข้อมูล ' . $user->getTable());
    //             }
    //         }
    //     } else {
    //         return $this->responseRequestError('ไม่พบข้อมูลผู้ใช้งาน');
    //     }
    // }
}
