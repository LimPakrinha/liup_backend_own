<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use App\Gender;

class GenderController extends MbsBaseController
{
    public function __construct()
    {
        parent::__construct(new Gender());
    }

    public function getAll(Request $request)
    {
        $request->request->add([
            'status_id' => Status::$ACTIVE
        ]);

        return parent::getAll($request);
    }

    public function getDataPaginate(Request $request, $limit)
    {
        $request->request->add([
            'status_id' => Status::$ACTIVE
        ]);

        return parent::getDataPaginate($request, $limit);
    }
}
