<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use App\Province;

class provinceController extends MbsBaseController
{
    public function __construct()
    {
        parent::__construct(new province());
    }

    public function getProvince(Request $request)
    {
        $condition = $this->setFilterCondition($request);
        $data = $this->model::where($condition)
            ->orderBy('prov_name', 'asc')
            ->where('status_id', Status::$ACTIVE)
            ->get();
            
        return $this->responseRequestSuccess($data);
    }
}
