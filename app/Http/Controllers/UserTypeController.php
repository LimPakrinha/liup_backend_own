<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use App\UserType;

class UserTypeController extends MbsBaseController
{
    public function __construct()
    {
        parent::__construct(new UserType());
    }

    public function getAll(Request $request)
    {
        $request->request->add([
            'status_id' => Status::$ACTIVE
        ]);

        return parent::getAll($request);
    }

    public function getDataPaginate(Request $request, $limit)
    {
        $request->request->add([
            'status_id' => Status::$ACTIVE
        ]);

        return parent::getDataPaginate($request, $limit);
    }
}
