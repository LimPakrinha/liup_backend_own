<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use App\AppType;

class AppTypeController extends MbsBaseController
{
    public function __construct()
    {
        parent::__construct(new AppType());
    }


}
