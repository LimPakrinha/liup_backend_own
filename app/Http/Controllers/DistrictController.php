<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use App\District;

class districtController extends MbsBaseController
{
    public function __construct()
    {
        parent::__construct(new district());
    }

    public function getDistrictData($pid)
    {
        $data = $this->model::where('province_id', $pid)
        ->orderBy('dist_name', 'asc')
        ->get();

    return $this->responseRequestSuccess($data);
    }

    public function getDataPaginate(Request $request, $limit)
    {
        $request->request->add([
            'status_id' => Status::$ACTIVE
        ]);

        return parent::getDataPaginate($request, $limit);
    }
}
