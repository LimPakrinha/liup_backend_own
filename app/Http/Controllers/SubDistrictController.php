<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use App\Subdistrict;

class subdistrictController extends MbsBaseController
{
    public function __construct()
    {
        parent::__construct(new subdistrict());
    }
    public function getSubDistrictData($did)
    {
        $data = $this->model::where('district_id', $did)
        ->orderBy('sub_district_name', 'asc')
        ->get();

    return $this->responseRequestSuccess($data);
    }

    public function getDataPaginate(Request $request, $limit)
    {
        $request->request->add([
            'status_id' => Status::$ACTIVE
        ]);

        return parent::getDataPaginate($request, $limit);
    }
}
