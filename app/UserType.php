<?php

namespace App;

class UserType extends MbsBaseModel
{
    protected $table = 'user_type';
    protected $fillable = [
        'platform_id', 'name',
    ];
    protected $hidden = [
        'platform_id', 'created_at', 'updated_at', 'status_id'
    ];

    public function __construct()
    {
        parent::__construct($this->table, $this->fillable);
    }
}
