<?php

namespace App;

class Zipcode extends MbsBaseModel
{
    protected $table = 'zipcode';
    protected $fillable = [
        'sub_district_id','zipcode'
    ];

    public function __construct()
    {
        parent::__construct($this->table, $this->fillable);
    }
}
