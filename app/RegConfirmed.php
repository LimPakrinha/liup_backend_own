<?php

namespace App;

class RegConfirmed extends MbsBaseModel
{
    protected $table = 'reg_confirmed';
    protected $fillable = [
        'name'
    ];
    protected $hidden = [
        'created_at', 'updated_at', 'status_id'
    ];

    public function __construct()
    {
        parent::__construct($this->table, $this->fillable);
    }
}
