<?php

namespace App;

class ExamType extends MbsBaseModel
{
    protected $table = 'exam_type';
    protected $fillable = [
        'name'
    ];
    protected $hidden = [
        'created_at', 'updated_at', 'status_id'
    ];

    public function __construct()
    {
        parent::__construct($this->table, $this->fillable);
    }
}
