<?php

namespace App;

class CardType extends MbsBaseModel
{
    protected $table = 'card_types';
    protected $fillable = [
        'id','card_name','status_id'
    ];

    public function __construct()
    {
        parent::__construct($this->table, $this->fillable);
    }
}
