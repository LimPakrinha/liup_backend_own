<?php

namespace App;

class Faculty extends MbsBaseModel
{
    protected $table = 'faculty';
    protected $fillable = [
        'id','faculty_name','status_id'
    ];

    public function __construct()
    {
        parent::__construct($this->table, $this->fillable);
    }
}
