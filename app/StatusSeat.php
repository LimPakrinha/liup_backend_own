<?php

namespace App;

class StatusSeat extends MbsBaseModel
{
    protected $table = 'status_seat';
    protected $fillable = [
        'name'
    ];

    public static $NOT_FULL = 1;
    public static $FULL = 2;

    public function __construct()
    {
        parent::__construct($this->table, $this->fillable);
    }
}
