<?php

namespace App;

class UserExam extends MbsBaseModel
{
    protected $table = 'user_exam';
    protected $fillable = [
        'user_id', 'exam_id', 'exam_date', 'receipt_file', 'seat_no', 'score', 'is_reg_confirmed', 'seat', 'is_pass','description'
    ];
    protected $hidden = [
        'created_at', 'updated_at', 'status_id'
    ];

    public function __construct()
    {
        parent::__construct($this->table, $this->fillable);
    }
}
