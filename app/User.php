<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class User extends MbsBaseModel implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table = 'user';
    protected $fillable = [
        'id', 'user_type_id','gender_id', 'card_no','department_id','level_id' ,'app_type_id','stu_id','title_id','other_title','first_name','last_name', 'email', 'profile_image', 'card_type_id', 'phone_number', 'address', 'sub_district_id'
    ];
    protected $hidden = [
        'password', 'created_at', 'updated_at', 'status_id'
    ];

    public function __construct()
    {
        parent::__construct($this->table, $this->fillable);
    }
}
