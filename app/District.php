<?php

namespace App;

class District extends MbsBaseModel
{
    protected $table = 'district';
    protected $fillable = [
        'id','dist_name','province_id','status_id'
    ];

    public function __construct()
    {
        parent::__construct($this->table, $this->fillable);
    }
}
