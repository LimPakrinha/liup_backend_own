<?php

namespace App;

class Department extends MbsBaseModel
{
    protected $table = 'department';
    protected $fillable = [
        'id','depart_name','faculty_id','status_id'
    ];

    public function __construct()
    {
        parent::__construct($this->table, $this->fillable);
    }
}
