<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RefAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->foreign('gender_id')->references('id')->on('gender');
            $table->foreign('status_id')->references('id')->on('status');
        });

        Schema::table('user_type', function (Blueprint $table) {
            $table->foreign('status_id')->references('id')->on('status');
        });

        Schema::table('user_user_type', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('user');
            $table->foreign('user_type_id')->references('id')->on('user_type');
            $table->foreign('status_id')->references('id')->on('status');
        });

        Schema::table('gender', function (Blueprint $table) {
            $table->foreign('status_id')->references('id')->on('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
