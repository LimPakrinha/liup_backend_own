<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 128);
            $table->timestamps();
        });

        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('gender_id');
            $table->string('username', 64)->unique();
            $table->string('password', 256);
            $table->string('name', 256);
            $table->string('email', 256);
            $table->string('profile_image', 256)->nullable();
            $table->date('birthday')->nullable();
            $table->string('phone_number', 64)->nullable();
            $table->text('address')->nullable();
            $table->decimal('latitude', 10, 8)->nullable();
            $table->decimal('longitude', 11, 8)->nullable();
            $table->timestamps();
            $table->unsignedInteger('status_id')->default(1);
        });

        Schema::create('user_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', '128');
            $table->timestamps();
            $table->unsignedInteger('status_id')->default(1);
        });

        Schema::create('user_user_type', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('user_type_id');
            $table->timestamps();
            $table->unsignedInteger('status_id')->default(1);
        });

        Schema::create('gender', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', '128');
            $table->timestamps();
            $table->unsignedInteger('status_id')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
