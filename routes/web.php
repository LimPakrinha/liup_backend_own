<?php

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/user/reset_password/{rpk}', 'UserController@resetPassword');

$router->group(['prefix' => 'api/v1'], function ($router) {
    $router->post('user/login', ['uses' => 'UserController@login']);
    $router->post('user/forgot_password', 'UserController@forgotPassword');

    $router->get('cardtype', 'CardTypeController@index');
    $router->get('apptype', 'AppTypeController@index');

    $router->get('title', 'TitleController@getAll');

    $router->get('faculty', 'FacultyController@getFaculty');
    $router->get('department/{fid}', 'DepartmentController@getDepartmentData');
    $router->get('department/{did}/faculty', 'DepartmentController@getData');

    $router->get('level', 'LevelController@getAll');


    $router->get('province', 'ProvinceController@index');
    $router->get('district/{pid}', 'DistrictController@getDistrictData');
    $router->get('subdistrict/{did}', 'SubDistrictController@getSubDistrictData');
    $router->get('zipcode/{sid}', 'ZipcodeController@getZipData');
    $router->post('register', 'UserController@addData');
});

$router->group(['prefix' => 'api/v1', 'middleware' => 'jwt.auth'], function ($router) {
    $router->get('user/{id}', 'UserController@getData');
    $router->post('user/{id}/profile_image', 'UserController@updateImageProfile');
    $router->put('user/{id}', 'UserController@updateData');
    $router->delete('user/{id}', 'UserController@deleteData');
    $router->get('user_type', 'UserTypeController@getAll');

    $router->get('gender', 'GenderController@getAll');

    $router->get('exam', 'ExamController@getAll');
    $router->post('exam/show', 'ExamController@getAllExam');
    $router->get('exam/{id}', 'ExamController@getData');
    $router->post('exam/user_register', 'ExamController@getAllUserRegister');
    $router->post('exam', 'ExamController@addData');
    $router->put('exam/{id}', 'ExamController@updateData');
    $router->delete('exam/{id}', 'ExamController@deleteData');

    $router->get('status_seat', 'Status_SeatController@getAll');

    $router->get('user_exam', 'UserExamController@getAll');
    $router->post('user_exam/user', 'UserExamController@getUserExam');
    $router->get('user_exam/{id}/confirmed', 'UserExamController@getExamRegConfirmed');
    $router->get('user_exam/{id}/user', 'UserExamController@getExamUser');
    $router->get('user_exam/{id}', 'UserExamController@getData');
    $router->post('user_exam/paginate_mq', 'UserExamController@getDataPaginateModelQuery');
    $router->post('user_exam', 'UserExamController@addData');
    $router->put('user_exam/{id}', 'UserExamController@updateData');

    $router->post('user_exam/score/{id}', 'UserExamController@getUserExamScore');

    $router->get('user_exam/detail/{id}', 'UserExamController@getUserExamScoreDetail');
    
    $router->get('exam_type', 'ExamTypeController@getAll');

    $router->post('user_admin/user/{id}', 'UserAdminController@getUser');
    $router->get('user_admin/{id}', 'UserAdminController@getUserAdminData');
    $router->delete('user_admin/{id}', 'UserAdminController@deleteData');
    $router->put('user_admin/{id}', 'UserAdminController@updateData');

    $router->post('user_client', 'UserClientController@getAllUserClient');
    $router->get('user_client/{id}', 'UserClientController@getClientDetail');
    $router->get('user_client/{id}/admin', 'UserClientController@getData');

    $router->put('user/{id}/pwd', 'UserController@updatePwd');

    $router->get('reg_confirmed', 'RegConfirmedController@getAll');
});
